import java.util.Scanner;
public class GameOX {
	static int row;
	static int col;
	static int check = 0;
	static Scanner input = new Scanner(System.in);
	static char[][] board = {
			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'}};
	
	static char player = 'X';
	public static void printWelcome() {
		System.out.println("Welcome To OX-Game");
	}
	public static void printTurn() {
		System.out.println();
		System.out.print(player +" "+ "Turn");
	}
	public static void printBye() {
		System.out.println();
		System.out.print("Bye Bye!!");
	}
	public static void UpdateBoard() {
		for(int i = 0 ; i<3 ;i++) {
			System.out.println();
			for(int j = 0 ; j<3 ;j++) {
				System.out.print(board[i][j]+ " | ");
			}
		}
	}
	public static void input() {
		while(true) {
			
			System.out.println();
			System.out.println("Enter row and column: ");
			row = input.nextInt()-1;
			col = input.nextInt()-1;
			if(CheckBox()) {
				System.out.println("You cannot input here");	
				continue ;	
			}
			break;
			
		}
		check= check +1;
	}
	public static void switchPlayer() {
		if (player == 'X')
			player = 'O';
		else 
			player = 'X';
	}
	public static boolean CheckDraw() {
		return check > 8;
		
	}
	public static boolean CheckBox() {
		return board[row][col] != '-';
		
	}
	public static void main(String[] args) {
		printWelcome();
		Play();
		printBye();
		
	}
	public static void Play() {
		boolean playing = true ;
		UpdateBoard();
		while(playing) {
			printTurn();
			input();
			
			board[row][col] = player;
			if (Checkwin(row,col)) {
				playing = false;
				System.out.println("Player:"+ player + " is Winner !");
				
			}
			else if(CheckDraw()) {
				System.out.println("DRAW !!");
				
				playing = false;
			}
			UpdateBoard();
			switchPlayer();
		}
		
	}
	
	public static boolean Checkwin(int rowM,int colM) {
		//�ǵ��
		if(board[0][colM] == board[1][colM] && board[0][colM] == board[2][colM]){
			check = 0;
			return true;	
		}
		//�ǹ͹
		if(board[rowM][0] == board[rowM][1] && board[rowM][0] == board[rowM][2]) {
			check = 0;
			return true;
		}
		//�Ƿ��§��� 
		if(board[0][0] == board[1][1] && board[0][0] == board[2][2] && board[1][1] != '-') {
			check = 0;
			return true;
		}
		if(board[0][2] == board[1][1] && board[0][2] == board[2][0] && board[1][1] != '-') {
			check = 0;
			return true;
		}
		
			
		return false;
	}
}